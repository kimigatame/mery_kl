#title="スニペットダイアログ"
#include "../InsertOption.js"
#include "../WidePop.js"

let r=new ComText(RelativePath("inserttext.json"),InsertOption).match(false);
if(r.options.length>0){
  let menu=[];
  for(let i=0;i<r.options.length;i++){
    menu.push([r.options[i].name,r.options[i]]);
  }
  menu=WidePop.normalize(menu);
  let result=new WidePop(menu).create().track(0,false).value;
  if(result){
    result.insert();
  }
}
