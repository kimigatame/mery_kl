#include "ComText.js"

let InsertOption=function(){
  ComText.Option.apply(this,arguments);
  /**
  * 挿入位置。
  * @type {ComText.Range}
  */
  this.insertRange=undefined;
  /**
  * 挿入後の選択位置。ComText.Generated#line#full#startを基準とする。
  * @type {ComText.Range}
  */
  this.selRange=undefined;
}
Object.setPrototypeOf(InsertOption.prototype,ComText.Option.prototype);
InsertOption.prototype.constructor=InsertOption;
//override
InsertOption.prototype.appendix=function(str){
  this.selRange=new ComText.Range(str.indexOf("{s}"),str.indexOf("{e}"));
  if(this.selRange.start==-1&&this.selRange.end==-1){
    this.selRange.start=str.length;
    this.selRange.end=this.selRange.start;
  }
  else if(this.selRange.start==-1){
    this.selRange.start=this.selRange.end;
  }
  else if(this.selRange.end==-1){
    this.selRange.end=this.selRange.start;
  }
  if(this.selRange.start>this.selRange.end){
    this.selRange.start-=3;
  }
  else if(this.selRange.start!=this.selRange.end){
    this.selRange.end-=3;
  }
  str=str.split("{s}");
  this.selRange.start-=(str[0].split(/\{[\{\}]\}/).length-1)*2;
  str=str.join("").split("{e}");
  this.selRange.end-=(str[0].split(/\{[\{\}]\}/).length-1)*2;
  return str.join();
}
/**
* マッチ結果に基づいて範囲を選択する。
*/
InsertOption.prototype.setInsertRange=function(){
  this.insertRange=ComText.parseRangeField(this.entry.range||"3,4",this.line,this.matchRanges);
  this.insertRange.start+=this.line.basePos;
  this.insertRange.end+=this.line.basePos;
  new KL.Selection().SetRange(this.insertRange.start,this.insertRange.end);
}
/**
* テキストを挿入する。
*/
InsertOption.prototype.insert=function(){
  this.setInsertRange();
  let $s=new KL.Selection();
  let t=Math.min(this.insertRange.start,this.insertRange.end);
  $s.Text=this.text;
  $s.SetRange(this.selRange.start+t,this.selRange.end+t);
}
