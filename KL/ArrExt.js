#title="Arrayオブジェクト拡張"

if(!Array.prototype.unique){
  /**
  * 配列から重複を削除する。
  * @return {Array.<*>}
  */
  Object.defineProperty(Array.prototype,"unique",{
    value:function(){
      let obj={};
      for(let i=0;i<this.length;){
        let s=JSON.stringify(this[i]);
        if(s in obj){
          this.splice(i,1);
        }
        else{
          obj[s]=1;
          i++;
        }
      }
      return this;
    }
  });
}
if(!Array.prototype.hasOwnProperty("last")){
  /**
  * 配列の最後の要素を取得する。
  * @return {*}
  */
  Object.defineProperty(Array.prototype,"last",{
    get:function(){
      return this[this.length-1];
    },
    set:function(value){
      this[this.length-1]=value;
    }
  });
}
