# About

テキストエディタ[Mery](http://www.haijin-boys.com/wiki)の用ライブラリです。
Mery組み込みオブジェクトのラッパやChakraに実装されていないECMAScriptのメソッドの補完などを含みます。

# Content

- polyfill

Chakraの補完です。

- ArrExt
- MathExt
- StrExt

ESに存在しないメソッド等の追加です。

- KL

主にMery組み込みオブジェクトのラッパです。組み込みのメソッドと独自拡張のメソッドを同一のオブジェクトから呼び出せます。
~~~
let $s=new KL.Selection();
if(!$s.IsEmpty){
  if($s.GetDirection()==-1){
    $s.Text=$s.Text.Sort();
  }
}
~~~

- global

KLに属さないグローバルな関数/変数です。

## Chakraに補完した機能

- Array
	- Array.from()
	- Array.of()
	- Array.prototype.copyWithin()
	- Array.prototype.fill()
	- Array.prototype.find()
	- Array.prototype.findIndex()
	- Array.prototype.flat()
	- Array.prototype.flatMap()
	- Array.prototype.includes()
- Math
	- Math.trunc()
- Number
	- Number.isFinite()
	- Number.isInteger()
	- Number.isNaN()
	- Number.parseFloat()
	- Number.parseInt()
- Object
	- Object.is()
- String
	- String.fromCodePoint()
	- String.prototype.codePointAt()
	- String.prototype.endsWith()
	- String.prototype.includes()
	- String.prototype.padEnd()
	- String.prototype.padStart()
	- String.prototype.repeat()
	- String.prototype.startsWith()
	- String.prototype.trimEnd()

# Lisence

## polyfill.js

内容の多くは[Mozilla における ECMAScript 6 のサポート](https://developer.mozilla.org/ja/docs/Web/JavaScript/ECMAScript_6_support_in_Mozilla)のPolyfillを使用しているので、同等の[CC0](https://creativecommons.org/publicdomain/zero/1.0/)とします。

## その他

MIT（旧バージョンのNYSLから変更していますが、実際のところどちらでも構いません）

Copyright 2019 黄身

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
