#title="Stringオブジェクト拡張"

if(!String.prototype.bracket){
  /**
  * 文字列の前後に文字列を挿入する。
  * @param {string} pre 先頭の文字列
  * @param {string} [post] 末尾の文字列。省略すればpreと同じ
  * @return {string}
  */
  Object.defineProperty(String.prototype,"bracket",{
    value:function(pre,post){
      if(!pre){
        return this;
      }
      if(!post){
        post=pre;
      }
      return pre+this+post;
    }
  });
}
if(!String.prototype.dquote){
  /**
  * 「"」で囲む。
  * @return {string}
  */
  Object.defineProperty(String.prototype,"dquote",{
    value:function(){
      return '"'+this+'"';
    }
  });
}
if(!String.prototype.quote){
  /**
  * 「'」で囲む。
  * @return {string}
  */
  Object.defineProperty(String.prototype,"quote",{
    value:function(){
      return "'"+this+"'";
    }
  });
}
if(!String.prototype.parens){
  /**
  * 「()」で囲む。
  * @return {string}
  */
  Object.defineProperty(String.prototype,"parens",{
    value:function(){
      return "("+this+")";
    }
  });
}
if(!String.prototype.braces){
  /**
  * 「[]」で囲む。
  * @return {string}
  */
  Object.defineProperty(String.prototype,"braces",{
    value:function(){
      return "["+this+"]";
    }
  });
}
if(!String.prototype.curls){
  /**
  * 「{}」で囲む。
  * @return {string}
  */
  Object.defineProperty(String.prototype,"curls",{
    value:function(){
      return "{"+this+"}";
    }
  });
}
if(!String.prototype.crlf){
  /**
  * 改行コードを変換する。
  * @param {number} [type=0] 0ならCRLFへ、1ならCRへ、2ならLFへ変換
  * @return {string}
  */
  Object.defineProperty(String.prototype,"crlf",{
    value:function(type){
      return this.replace(/\u000d?\u000a|\u000d/g,type==1?"\u000d":type?"\u000a":"\u000d\u000a");
    }
  });
}
if(!String.prototype.insert){
  /**
  * 文字列を挿入する。
  * @param {string} seg 挿入する文字列
  * @param {!number} pos 挿入位置
  * @return {string}
  */
  Object.defineProperty(String.prototype,"insert",{
    value:function(seg,pos){
      return this.substring(0,pos)+seg+this.substr(pos);
    }
  });
}
if(!String.prototype.escapeRe){
  /**
  * 正規表現で使用される文字をエスケープする。
  * @return {string}
  */
  Object.defineProperty(String.prototype,"escapeRe",{
    value:function(){
      return this.replace(/[\\\*\+\.\?\{\}\(\)\[\]\^\$\-\|]/g,"\\$&");
    }
  });
}
if(!String.prototype.unescapeRe){
  /**
  * エスケープされた正規表現で使用される文字を戻す。
  * @return {string}
  */
  Object.defineProperty(String.prototype,"unescapeRe",{
    value:function(){
      return this.replace(/\\(?=[\\\*\+\.\?\{\}\(\)\[\]\^\$\-\|])/g,"");
    }
  });
}
if(!String.prototype.cpcount){
  /**
  * サロゲートペアを考慮して文字列の長さを取得する。
  * @return {number}
  */
  Object.defineProperty(String.prototype,"cpcount",{
    value:function(){
      let count=0;
      let high=false;
      for(let i=0;i<this.length;i++){
        let code=this.charCodeAt(i);
        if(0xd800<=code&&code<=0xdbff){
          count++;
          high=true;
        }
        else if(!(0xdc00<=code&&code<=0xdfff)||!high){
          count++;
          high=false;
        }
      }
      return count;
    }
  });
}
if(!String.prototype.cpsplit){
  /**
  * サロゲートペアを考慮して文字列を分割する。
  * @return {Array.<string>}
  */
  Object.defineProperty(String.prototype,"cpsplit",{
    value:function(){
      let result=[];
      let high=false;
      for(let i=0;i<this.length;i++){
        let code=this.charCodeAt(i);
        if(0xd800<=code&&code<=0xdbff){
          result.push(this.charAt(i));
          high=true;
        }
        else if(0xdc00<=code&&code<=0xdfff&&high){
          result[result.length-1]+=String.fromCharCode(code);
          high=false;
        }
        else{
          result.push(this.charAt(i));
          high=false;
        }
      }
      return result;
    }
  });
}
