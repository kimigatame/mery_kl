#title="Numberオブジェクト拡張"

if(!Math.within){
  /**
  * n以上m以下であるか判定する。
  * @param {!number} value 判定する値
  * @param {!number} min 最小値
  * @param {!number} max 最大値
  * @return {boolean}
  */
  Math.within=function(value,min,max){
    return min<=value&&value<=max;
  }
}
if(!Math.clamp){
  /**
  * 数値を範囲内に収める。
  * @param {!number} value 収める値
  * @param {!number} min 最小値
  * @param {!number} max 最大値
  * @return {number}
  */
  Math.clamp=function(value,min,max){
    return Math.min(Math.max(value,min),max);
  }
}
