#title="グローバルな定数/関数"

if(meGetLineLogical===undefined){
  /**
  * 論理座標で指定する。
  * @global
  * @constant {number}
  */
  var meGetLineLogical=0;
}
if(mePosCaret===undefined){
  /**
  * キャレット位置で指定する。
  * @global
  * @constant {number}
  */
  var mePosCaret=0;
}
if(GEO===undefined){
  /**
  * 組み込みのEditorオブジェクトを取得する。
  * @param {number|Editor} editor エディタのインデックス/Editorオブジェクト。不正な値の場合はアクティブ
  * @return {Editor}
  */
  var GEO=function(editor){
    if(editor&&typeof(editor)=="object"&&"FindInFiles" in editor&&"ReplaceInFiles" in editor){
      return editor;
    }
    editor=parseInt(editor);
    return isNaN(editor)||editor<0||editor>=Editors.Count?Editor:Editors.Item(editor);
  }
}
if(GDO===undefined){
  /**
  * 組み込みのDocumentオブジェクトを取得する。
  * @param {number|string|Document} doc 文書のインデックス/ファイル名/Documentオブジェクト。不正な値の場合はアクティブ
  * @param {number|Editor} editor エディタのインデックス/Editorオブジェクト。不正な値の場合はアクティブ
  * @return {Document}
  */
  var GDO=function(doc,editor){
    if(doc&&typeof(doc)=="object"&&"CopyFullName" in doc&&"HighlightFind" in doc){
      return doc;
    }
    let $e=GEO(editor);
    if(doc instanceof String||typeof(doc)=="string"){
      for(let i=0;i<$e.Documents.Count;i++){
        let $d=$e.Documents.Item(i);
        if($d.Name==doc||$d.FullName==doc){
          return $d;
        }
      }
    }
    let num=parseInt(doc);
    return isNaN(num)||num<0||num>=$e.Documents.Count?$e.ActiveDocument:$e.Documents.Item(num);
  }
}
if(GSO===undefined){
  /**
  * 組み込みのSelectionを取得する。
  * @param {number|string|Document} doc 文書のインデックス/ファイル名/Documentオブジェクト。不正な値の場合はアクティブ
  * @param {number|Editor} editor エディタのインデックス/Editorオブジェクト。不正な値の場合はアクティブ
  * @return {Selection}
  */
  var GSO=function(doc,editor){
    return GDO(doc,editor).Selection;
  }
}
if(O===undefined){
  /**
  * OutputBar.Writeへのショートカット。
  * @param {?object} data 出力データ
  * @param {number} flags 1:改行しない、2:出力前にウィンドウをクリアする、4:出力ウィンドウにフォーカスを移す
  */
  var O=function(data,flags){
    OutputBar.Visible=true;
    if(flags&2){
      OutputBar.Clear();
    }
    switch(data){
      case null:
        data="*null";
        break;
      case undefined:
        data="*undefined";
        break;
      default:
        try{
          data=data.toString();
        }
        catch(e){
          data=typeof(data)=="object"?"[object Object]":data;
        }
    }
    OutputBar.Write(data+(flags&1?"":"\n"));
    if(flags&4){
      OutPutBar.SetFocus();
    }
  }
}
if(RelativePath===undefined){
  /**
  * 実行中のスクリプトから相対的にファイルパスを取得する。
  * @param {string} name ファイル名。ディレクトリもここで指定する
  * @param {number} [rank=0] 遡る階層の数
  */
  var RelativePath=function(name,rank){
    rank=rank||0;
    return ScriptFullName.split("\\").slice(0,-(rank+1)).join("\\")+"\\"+name;
  }
}