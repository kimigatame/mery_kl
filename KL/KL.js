#include "global.js"

var KL;
if(KL==null){
  /*
  * KILIからの変更点
  * KILI.Range => KL.Range
  *   プロパティをメソッドに変更
  *   座標系はKL.Pointで指定
  * KILI.LineMode => KL.Coordに統合
  * KILI.Direction => KL.Dir
  * KILI.Coordinate => KL.Coord
  *   posをserialに変更
  *   logicalとviewはmePosViewとmePosLogicalに数値を合わせた
  * KILI.Point => KL.Point
  *   coordを追加
  * KILI.selex,KILI.edit,KILI.docex => KL.SelectionまたはKL.DocumentまたはKL.Editor
  *   組み込みのインターフェースを拡張する形に変更し、追加したメソッドも組み込みと一律に扱えるようにした
  *   引数のDocumentが不要になった
  *   行モードは基本的にKL.Point.coordで指定
  * @class
  * @static
  */
  KL={};
  /**
  * 選択範囲を表す。
  * @param {KL.Point} active 選択終了位置
  * @param {KL.Point} anchor 選択開始位置
  * @param {KL.Document} $s 対象ドキュメントのKL.Document
  * @class
  */
  KL.Range=function(anchor,active,$d){
    /**
    * 選択終了位置を取得する。
    * @return {KL.Point}
    */
    this.anchor=anchor;
    /**
    * 選択開始位置を取得する。
    * @return {KL.Point}
    */
    this.active=active;
    /**
    * 選択方向。
    * @type {KL.Dir}
    */
    this.direction=(function(){
      let anc=$d.ToSerial(anchor);
      let act=$d.ToSerial(active);
      if(anc.x<act.x){
        return KL.Dir.forward;
      }
      if(anc.x>act.x){
        return KL.Dir.backward;
      }
      return KL.Dir.none;
    })();
    /**
    * 文頭側の位置を取得する。
    * @return {KL.Point}
    */
    Object.defineProperty(this,"top",{
      get:function(){
        return this.direction==KL.Dir.backward?this.active:this.anchor;
      },
      set:function(value){
        if(this.direction==KL.Dir.backward){
          this.active=value;
        }
        else{
          this.anchor=value;
        }
      }
    });
    /**
    * 文末側の位置を取得する。
    * @return {KL.Point}
    */
    Object.defineProperty(this,"bottom",{
      get:function(){
        return this.direction==KL.Dir.backward?this.anchor:this.active;
      },
      set:function(value){
        if(this.direction==KL.Dir.backward){
          this.anchor=value;
        }
        else{
          this.active=value;
        }
      }
    });
    Object.seal(this);
  }
  /**
  * 文字列で表現する。
  * @return {string}
  */
  KL.Range.prototype.toString=function(){
    return "Anchor: {"+this.anchor.toString()+"}, Active: {"+this.active.toString()+"}, Dir: "+this.direction;
  }
  KL.Range.prototype.reverse=function(){
    let tmp=this.anchor;
    this.anchor=this.active;
    this.active=tmp;
    this.direction*=-1;
  }
  /**
  * 選択方向。
  * @enum {number}
  */
  KL.Dir={
    /**
    * 文頭方向。
    */
    backward:-1,
    /**
    * 選択なし。
    */
    none:0,
    /**
    * 文末方向。
    */
    forward:1,
  };
  Object.freeze(KL.Dir);
  /**
  * 座標系。
  * @enum {number}
  */
  KL.Coord={
    /**
    * 文頭からの位置。
    * 行モードの指定として使う場合は全体を単一行として扱う。範囲指定では全体を表す
    */
    serial:0,
    /**
    * 表示行。
    */
    view:1,
    /**
    * 論理行。
    */
    logical:2,
  };
  Object.freeze(KL.Coord);
  /**
  * 座標を表す。
  * @class
  * @param {number} x X座標==桁
  * @param {number} y Y座標==行。KL.Coord.serialの時は常に1
  * @param {KL.Coord} [coord=KL.Coord.logical] 座標系
  */
  KL.Point=function(x,y,coord){
    /**
    * X座標。
    * @type {number}
    */
    this.x=x<0?-1:x;
    /**
    * Y座標。
    * @type {number}
    */
    this.y=y<0?-1:y;
    /**
    * 座標系。
    * @type {KL.Coord}
    */
    this.coord=coord==null?KL.Coord.logical:coord;
    Object.seal(this);
  }
  /**
  * 文字列で表現する。
  * @return {string}
  */
  KL.Point.prototype.toString=function(){
    return "X: "+this.x+", Y: "+this.y+", Coord: "+this.coord;
  }

  /*
  * Documentsコレクションの拡張クラス。
  * @param {Editor} editor 対象のEditor
  * @class
  */
  KL.Documents=function(editor){
    Object.defineProperty(this,"Count",{
      get:function(){
        return editor.Documents.Count;
      }
    });
    Object.defineProperty(this,"Item",{
      value:function(value){
        return new KL.Document(editor.Documents.Item(value));
      }
    });
  }

  /*
  * Editorオブジェクトの拡張クラス。
  * @param {number|Editor} editor エディタのインデックス。もしくはEditorオブジェクト。不正な値の場合はアクティブ
  * @class
  */
  KL.Editor=function(doc,editor){
    /*
    * 組み込みのEditorオブジェクト。
    */
    Object.defineProperty(this,"Original",{
      value:editor!=null&&editor instanceof KL.Editor?editor.Original:GEO(editor)
    });
    /*
    * KL.Documentsを取得する。
    * 組み込みのDocumentsはthis.Original.Documentsで取得できる
    * @return {KL.Documents}
    */
    Object.defineProperty(this,"Documents",{
      value:new KL.Documents(this.Original)
    });
  }
  Object.defineProperty(KL.Editor.prototype,"FindInFiles",{
    value:function(findPattern,path,flags){
      return this.Original.FindInFiles(findPattern,path,flags);
    }
  });
  Object.defineProperty(KL.Editor.prototype,"OpenFile",{
    value:function(file,encoding,flags){
      return this.Original.OpenFile(file,encoding,flags);
    }
  });
  Object.defineProperty(KL.Editor.prototype,"ReplaceInFiles",{
    value:function(findPattern,replacePattern,path,flags){
      return this.Original.ReplaceInFiles(findPattern,replacePattern,path,flags);
    }
  });
  Object.defineProperty(KL.Editor.prototype,"SaveAll",{
    value:function(){
      return this.Original.SaveAll();
    }
  });
  Object.defineProperty(KL.Editor.prototype,"SaveCloseAll",{
    value:function(){
      return this.Original.SaveCloseAll();
    }
  });
  if(parseInt(Editor.Version[0])>2){
    Object.defineProperty(KL.Editor.prototype,"ReadSettings",{
      value:function(){
        return this.Original.ReadSettings();
      }
    });
    Object.defineProperty(KL.Editor.prototype,"ReadSettingInteger",{
      value:function(section,ident,dflt){
        return this.Original.ReadSettingInteger(section,ident,dflt);
      }
    });
    Object.defineProperty(KL.Editor.prototype,"ReadSettingString",{
      value:function(section,ident,dflt){
        return this.Original.ReadSettingString(section,ident,dflt);
      }
    });
    Object.defineProperty(KL.Editor.prototype,"WriteSettings",{
      value:function(){
        return this.Original.WriteSettings();
      }
    });
    Object.defineProperty(KL.Editor.prototype,"WriteSettingInteger",{
      value:function(section,ident,value){
        return this.Original.WriteSettingInteger(section,ident,value);
      }
    });
    Object.defineProperty(KL.Editor.prototype,"WriteSettingString",{
      value:function(section,ident,value){
        return this.Original.WriteSettingString(section,ident,value);
      }
    });
  }
  /*
  * アクティブな文書のKL.Documentを取得する。
  * 組み込みのActiveDocumentはthis.Original.ActiveDocumentで取得できる
  * @return {KL.Document}
  */
  Object.defineProperty(KL.Editor.prototype,"ActiveDocument",{
    get:function(){
      return new KL.Document(this.Original.ActiveDocument);
    }
  });
  Object.defineProperty(KL.Editor.prototype,"EnableTab",{
    get:function(){
      return this.Original.EnableTab;
    }
  });
  Object.defineProperty(KL.Editor.prototype,"FullName",{
    get:function(){
      return this.Original.FullName;
    }
  });
  Object.defineProperty(KL.Editor.prototype,"Version",{
    get:function(){
      return this.Original.Version;
    }
  });
  if(parseInt(Editor.Version[0])>2){
    Object.defineProperty(KL.Editor.prototype,"Tag",{
      get:function(){
        return this.Original.Tag;
      },
    });
  }
  /**
  * 文書を新規作成する。
  * @param {?string} text
  * @param {number} [encoding=meEncodingNone]
  * @param {string} [mode="Text"]
  * @param {boolean} [readOnly=false]
  * @param {boolean} [overwrite=false]
  * @param {Editor} [destination] 文書作成先のEditorオブジェクト。指定しなければアクティブ
  * @return {KL.Document} 作成した文書のDocumentオブジェクト
  */
  Object.defineProperty(KL.Editor.prototype,"NewFile",{
    value:function(text,encoding,mode,readOnly,overwrite,editor){
      editor=GEO(editor);
      Redraw=false;
      editor.NewFile();
      let $d=new KL.Document(editor.ActiveDocument);
      if(arguments.length){
        $d.Encoding=encoding||meEncodingNone;
        $d.Mode=mode||"Text";
        $d.ReadOnly=readOnly;
        $d.Selection.Text=text;
        $d.Selection.OverwriteMode=overwrite;
      }
      Redraw=true;
      return $d;
    }
  });
  /*
  * Documentオブジェクトを取得する。
  * @param {!number} index 文書のインデックス
  * @param {boolean} [kl=false] KL.Documentオブジェクトで取得する
  * @return {Document}
  */
  Object.defineProperty(KL.Editor.prototype,"GetDocument",{
    value:function(index,kl){
      return kl?new KL.Document(this.Documents.Item(index)):this.Documents.Item(index);
    }
  });
  /**
  * 文書を全て閉じる。
  * @param {number} [save=0] -1:変更を破棄、0:変更があれば確認、1:変更を保存
  * @param {number} [saved=0] -1:新規ファイルのみ、0:全て、1:既存ファイルのみ
  */
  Object.defineProperty(KL.Editor.prototype,"CloseAll",{
    value:function(save,saved){
      let docs=this.Documents;
      for(let i=0;i<docs.Count;i++){
        if(!saved||saved<0&&docs.Item(i).Name==""||saved>0&&docs.Item(i).Name!=""){
          this.GetDocument(i,true).Close(save);
        }
      }
    }
  });
  /**
  * 描画を止めて処理を行い、終了後選択範囲とスクロール位置を復帰する。
  * @param {!function():*} func
  * @return {*}
  */
  //KL.Editor.prototype.Define("Suspend",function(func){
  //  Redraw=false;
  //  let sel=new KL.Selection(this.$.ActiveDocument);
  //  let range=sel.GetRange();
  //  let scroll=KL.GetScroll();
  //  let result=func();
  //  sel.SetRange(range);
  //  KL.SetScroll(scroll);
  //  Redraw=true;
  //  return result;
  //});
  /*
  * Documentオブジェクトの拡張クラス。
  * @param {number|Document} doc 文書のインデックス。もしくはDocumentオブジェクト。不正な値の場合はアクティブ
  * @param {number|Editor} editor エディタのインデックス。もしくはEditorオブジェクト。不正な値の場合はアクティブ
  * @class
  */
  KL.Document=function(doc,editor){
    /*
    * 組み込みのDocumentオブジェクト。
    */
    Object.defineProperty(this,"Original",{
      value:doc!=null&&doc instanceof KL.Document?doc.Original:GDO(doc,editor)
    });
  }
  Object.defineProperty(KL.Document.prototype,"Activate",{
    value:function(){
      return this.Original.Activate();
    }
  });
  /**
  * 文書を閉じる。
  * @param {number} [save=0] -1:変更を破棄、0:変更があれば確認、1:変更を保存
  * @param {string} [file] ファイル名
  */
  Object.defineProperty(KL.Document.prototype,"Close",{
    value:function(save,file){
      if(save<0){
        this.Original.Saved=true;
      }
      else if(save>0){
        this.Original.Save(file);
      }
      this.Original.Close();
    }
  });
  Object.defineProperty(KL.Document.prototype,"CopyFullName",{
    value:function(){
      return this.Original.CopyFullName();
    }
  });
  Object.defineProperty(KL.Document.prototype,"CopyPath",{
    value:function(){
      return this.Original.CopyPath();
    }
  });
  Object.defineProperty(KL.Document.prototype,"GetLine",{
    value:function(line,flags){
      return this.Original.GetLine(line,flags);
    }
  });
  Object.defineProperty(KL.Document.prototype,"GetLines",{
    value:function(flags){
      return this.Original.GetLines(flags);
    }
  });
  Object.defineProperty(KL.Document.prototype,"Redo",{
    value:function(){
      return this.Original.Redo();
    }
  });
  Object.defineProperty(KL.Document.prototype,"Save",{
    value:function(){
        return this.Original.Save();
    }
  });
  Object.defineProperty(KL.Document.prototype,"Undo",{
    value:function(){
      return this.Original.Undo();
    }
  });
  Object.defineProperty(KL.Document.prototype,"Write",{
    value:function(text){
      return this.Original.Write(text);
    }
  });
  Object.defineProperty(KL.Document.prototype,"Writeln",{
    value:function(text){
      return this.Original.Writeln(text);
    }
  });
  /*
  * KL.Selectionを取得する。
  * 組み込みのSelectionはthis.Original.Selectionで取得できる
  * @return {KL.Selection}
  */
  Object.defineProperty(KL.Document.prototype,"Selection",{
    get:function(){
      return new KL.Selection(this);
    }
  });
  Object.defineProperty(KL.Document.prototype,"Encoding",{
    get:function(){
      return this.Original.Encoding;
    },
    set:function(value){
      return this.Original.Encoding=value;
    }
  });
  Object.defineProperty(KL.Document.prototype,"FullName",{
    get:function(){
      return this.Original.FullName;
    }
  });
  Object.defineProperty(KL.Document.prototype,"HighlightFind",{
    get:function(){
      return this.Original.HighlightFind;
    },
    set:function(value){
      return this.Original.HighlightFind=value;
    }
  });
  Object.defineProperty(KL.Document.prototype,"LineEnding",{
    get:function(){
      return this.Original.LineEnding;
    },
    set:function(value){
      return this.Original.LineEnding=value;
    }
  });
  Object.defineProperty(KL.Document.prototype,"Mode",{
    get:function(){
      return this.Original.Mode;
    },
    set:function(value){
      return this.Original.Mode=value;
    }
  });
  Object.defineProperty(KL.Document.prototype,"Name",{
    get:function(){
      return this.Original.Name;
    }
  });
  Object.defineProperty(KL.Document.prototype,"Path",{
    get:function(){
      return this.Original.Path;
    }
  });
  Object.defineProperty(KL.Document.prototype,"ReadOnly",{
    get:function(){
      return this.Original.ReadOnly
    },
    set:function(value){
      return this.Original.ReadOnly=value;
    }
  });
  Object.defineProperty(KL.Document.prototype,"Saved",{
    get:function(){
      return this.Original.Saved;
    },
    set:function(value){
      return this.Original.Saved=value;
    }
  });
  Object.defineProperty(KL.Document.prototype,"Text",{
    get:function(){
      return this.Original.Text;
    },
    set:function(value){
      return this.Original.Text=value;
    }
  });
  if(parseInt(Editor.Version[0])>2){
    Object.defineProperty(KL.Document.prototype,"MaxLineLength",{
      get:function(){
        return this.Original.MaxLineLength;
      },
      set:function(value){
        return this.Original.MaxLineLength=value;
      }
    });
    Object.defineProperty(KL.Document.prototype,"Tag",{
      get:function(){
        return this.Original.Tag;
      },
    });
    Object.defineProperty(KL.Document.prototype,"TextLength",{
      get:function(){
        return this.Original.TextLength;
      },
    });
  }
  /*
  * 改行文字を取得する。
  * @return {string}
  */
  Object.defineProperty(KL.Document.prototype,"LineEndingChar",{
    get:function(){
      return this.Original.LineEnding==0?"\r\n":this.Original.LineEnding==1?"\r":"\n";
    }
  });
  /*
  * 座標を文頭からの位置に変換する。
  * @param {!KL.Point} point 座標
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Document.prototype,"ToSerial",{
    value:function(point){
      if(point.coord==KL.Coord.serial){
        return point;
      }
      if(point.coord==KL.Coord.logical){
        return this.LogicalToSerial(point);
      }
      return this.ViewToSerial(point);
    }
  });
  /*
  * 座標を論理行座標に変換する。
  * @param {!KL.Point} point 座標
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Document.prototype,"ToLogical",{
    value:function(point){
      if(point.coord==KL.Coord.logical){
        return point;
      }
      if(point.coord==KL.Coord.serial){
        return this.SerialToLogical(point);
      }
      return this.ViewToLogical(point);
    }
  });
  /*
  * 座標を表示行座標に変換する。
  * @param {!KL.Point} point 座標
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Document.prototype,"ToView",{
    value:function(point){
      if(point.coord==KL.Coord.view){
        return point;
      }
      if(point.coord==KL.Coord.serial){
        return this.SerialToView(point);
      }
      return this.LogicalToView(point);
    }
  });
  /**
  * 論理行座標を表示行座標に変換する。
  * @param {!KL.Point|number} x 論理行の座標または論理行桁
  * @param {number} [y] @xがnumberのとき、論理行
  * @param {boolean} [start=false] 折り返し位置の場合、表示行頭とみなす
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Document.prototype,"LogicalToView",{
    value:function(x,y,start){
      return this.SerialToView(this.LogicalToSerial(x,y),start);
    }
  });
  /**
  * 論理行座標を文頭からの位置に変換する。<br />
  * 論理行桁が末尾を越える場合は末尾とみなす。
  * @param {!KL.Point|number} x 論理行の座標または論理行桁
  * @param {number} [y] @xがnumberのとき、論理行
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Document.prototype,"LogicalToSerial",{
    value:function(x,y){
      if(x instanceof KL.Point){
        y=x.y;
        x=x.x;
      }
      let text=this.Original.Text.split("\n").slice(0,y);
      return new KL.Point(text.join("\n").length-(text[text.length-1].length-(Math.min(text[text.length-1].length+1,x)-1)),1,KL.Coord.serial);
    }
  });
  /**
  * 文頭からの位置を論理行座標に変換する。
  * @param {!KL.Point|number} point 文頭からの位置
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Document.prototype,"SerialToLogical",{
    value:function(point){
      if(point instanceof KL.Point){
        point=point.x
      }
      let logicals=this.Original.Text.slice(0,point).split("\n");
      return new KL.Point(logicals.last.length+1,logicals.length,KL.Coord.logical);
    }
  });
  /**
  * 文頭からの位置を表示行座標に変換する。
  * @param {!KL.Point|number} point 文頭からの位置
  * @param {boolean} [start=false] 折り返し位置の場合、表示行頭とみなす
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Document.prototype,"SerialToView",{
    value:function(point,start){
      if(point instanceof KL.Point){
        point=point.x;
      }
      let text=this.Original.Text;
      point=Math.min(Math.max(point,0),text.length);
      let total=0;
      let lineCount=this.Original.GetLines(meGetLineView);
      for(let i=1;i<lineCount;i++){
        let v=this.Original.GetLine(i,meGetLineView).length;
        if(total+v>=point){
          break;
        }
        total+=v;
        if(text.charAt(total)=="\n"){
          total+=1;
        }
      }
      if(start&&point==v+total&&text.charAt(v+total)!="\n"){
        return new KL.Point(1,i+1,KL.Coord.view);
      }
      return new KL.Point(point-total+1,i,KL.Coord.view);
    }
  });
  /**
  * 表示行座標を論理行座標に変換する。
  * @param {!KL.Point|number} x 表示行の座標または表示行桁
  * @param {number} [y] @xがnumberのとき、表示行
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Document.prototype,"ViewToLogical",{
    value:function(x,y){
      return this.SerialToLogical(this.ViewToSerial(x,y));
    }
  });
  /**
  * 表示行座標を文書先頭からの位置に変換する。<br />
  * 表示行桁が末尾を越える場合は末尾とみなす。
  * @param {!KL.Point|number} x 表示行の座標または表示行桁
  * @param {number} [y] @xがnumberのとき、表示行
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Document.prototype,"ViewToSerial",{
    value:function(x,y){
      if(x instanceof KL.Point){
        y=x.y;
        x=x.x;
      }
      x=Math.max(x,1);
      y=Math.min(Math.max(y,1),this.Original.GetLines(meGetLineView));
      let text=this.Original.Text;
      let i=1;
      let total=0;
      for(let j=1;j<y;j++){
        let v=this.Original.GetLine(j,meGetLineView).length;
        total+=v;
        if(text.charAt(total)=="\n"){
          total+=1;
        }
      }
      return new KL.Point(total+Math.min(this.Original.GetLine(y,meGetLineView).length+1,x)-1,1,KL.Coord.serial);
    }
  });
  /**
  * 文書のクローンを作成する。
  * @param {KL.Document||Document} [doc] 対象となるDocument。指定しなければアクティブ
  * @param {KL.Editor||Editor} [editor] 文書作成先のEditor。指定しなければアクティブ
  * @param {string} [text] 指定した場合テキストを置き換える
  */
  Object.defineProperty(KL.Document.prototype,"Clone",{
    value:function(doc,editor,text){
      editor=new KL.Editor(editor);
      doc=new KL.Document(doc);
      if(text==undefined){
        let range=doc.Selection.GetRange(KL.Coord.logical);
        let scroll=KL.GetScroll();
      }
      let newDoc=editor.NewFile(text==undefined?doc.Text:text,doc.Encoding,doc.Mode,doc.ReadOnly,doc.Selection.OverwriteMode);
      if(text==undefined){
        newDoc.Selection.SetRange(range,null,null);
        KL.SetScroll(scroll);
      }
      return newDoc;
    }
  });
  /**
  * 行を取得する。
  * @param {KL.Coord|undefined} [mode=KL.Coord.logical] 行モード
  * @param {number} newLine 0:改行を取得しない、1:論理行末にのみ改行を追加する、2:表示行末にも改行を追加する
  * @param {number} [from] 開始行。省略された場合全ての行を取得する
  * @param {number} [to] 最終行
  * @return {Array.<string>}
  */
  Object.defineProperty(KL.Document.prototype,"Lines",{
    value:function(mode,newLine,from,to){
      let arr=[];
      if(mode==undefined){
        mode=KL.Coord.logical;
      }
      if(mode==KL.Coord.serial){
        arr=[this.Original.Text];
      }
      else{
        if(from==undefined){
          from=1;
          to=this.Original.GetLines(mode);
        }
        from=Math.min(Math.max(from,1),this.Original.GetLines(mode==KL.Coord.logical?meGetLineLogical:meGetLineView));
         if(mode==KL.Coord.logical){
          arr=this.Original.Text.split("\n").slice(from-1,to);
          if(newLine>0){
            for(let i=0;i<arr.length;i++){
              arr[i]+="\n";
            }
            if(arr.last=="\n"){
              arr[arr.length-1]="";
            }
          }
        }
        else if(mode==KL.Coord.view){
          let text=this.Original.Text;
          let total=0;
          for(let i=from;i<=to;i++){
            let line=this.Original.GetLine(i,meGetLineView);
            if(newLine==1){
              total+=line.length;
              if(text.charAt(total)=="\n"){
                line+="\n";
                total+=1;
              }
            }
            else if(newLine==2){
              line+="\n";
            }
            arr.push(line);
          }
          if(arr.last=="\n"){
            arr[arr.length-1]="";
          }
        }
      }
      return arr;
    }
  });
  /*
  * Selectionオブジェクトの拡張クラス。
  * @param {number|Document} doc 文書のインデックス。もしくはDocumentオブジェクト。不正な値の場合はアクティブ
  * @param {number|Editor} editor エディタのインデックス。もしくはEditorオブジェクト。不正な値の場合はアクティブ
  * @class
  */
  KL.Selection=function(doc,editor){
    /*
    * 親となる組み込みのDocumentオブジェクト。
    */
    Object.defineProperty(this,"OriginalParent",{
      value:doc instanceof KL.Document?doc.Original:GDO(doc,editor)
    });
    /*
    * 親となるKL.Documentオブジェクト。
    */
    Object.defineProperty(this,"Parent",{
      value:doc instanceof KL.Document?doc:new KL.Document(doc)
    });
    /*
    * 組み込みのSelectionオブジェクト。
    */
    Object.defineProperty(this,"Original",{
      value:this.OriginalParent.Selection
    });
  }
  Object.defineProperty(KL.Selection.prototype,"ChangeCase",{
    value:function(flags){
      this.Original.ChangeCase(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"ChangeWidth",{
    value:function(flags){
      return this.Original.ChangeWidth(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"CharLeft",{
    value:function(extend,count){
      return this.Original.CharLeft(extend,count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"CharRight",{
    value:function(extend,count){
      return this.Original.CharRight(extend,count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"ClearBookmark",{
    value:function(){
      return this.Original.ClearBookmark();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Collapse",{
    value:function(flags){
      return this.Original.Collapse(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Copy",{
    value:function(){
      return this.Original.Copy();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"CopyLink",{
    value:function(){
      return this.Original.CopyLink();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Cut",{
    value:function(){
      return this.Original.Cut();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Delete",{
    value:function(count){
      return this.Original.Delete(count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"DeleteLeft",{
    value:function(count){
      return this.Original.DeleteLeft(count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"DestructiveInsert",{
    value:function(text){
      return this.Original.DestructiveInsert(text);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"DuplicateLine",{
    value:function(){
      return this.Original.DuplicateLine();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"EndOfDocument",{
    value:function(extend){
      return this.Original.EndOfDocument(extend);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"EndOfLine",{
    value:function(extend,flags){
      return this.Original.EndOfLine(extend,flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Find",{
    value:function(pattern,flags){
      return this.Original.Find(pattern,flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"FindRepeat",{
    value:function(flags){
      return this.Original.FindRepeat(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Format",{
    value:function(flags){
      return this.Original.Format(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetActivePointX",{
    value:function(flags,sel){
      return this.Original.GetActivePointX(flags,sel);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetActivePointY",{
    value:function(flags,sel){
      return this.Original.GetActivePointY(flags,sel);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetActivePos",{
    value:function(sel){
      return this.Original.GetActivePos(sel);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetAnchorPointX",{
    value:function(flags,sel){
      return this.Original.GetAnchorPointX(flags,sel);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetAnchorPointY",{
    value:function(flags,sel){
      return this.Original.GetAnchorPointY(flags,sel);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetAnchorPos",{
    value:function(sel){
      return this.Original.GetAnchorPos(sel);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetBottomPointX",{
    value:function(flags){
      return this.Original.GetBottomPointX(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetBottomPointY",{
    value:function(flags){
      return this.Original.GetBottomPointY(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetTopPointX",{
    value:function(flags){
      return this.Original.GetTopPointX(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"GetTopPointY",{
    value:function(flags){
      return this.Original.GetTopPointY(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Indent",{
    value:function(count){
      return this.Original.Indent(count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"InsertDate",{
    value:function(flags){
      return this.Original.InsertDate(flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"InsertFromFile",{
    value:function(file,encoding){
      return this.Original.InsertFromFile(file,encoding);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"LineDown",{
    value:function(extend,count){
      return this.Original.LineDown(extend,count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"LineOpen",{
    value:function(above){
      return this.Original.LineOpen(above);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"LineUp",{
    value:function(extend,count){
      return this.Original.LineUp(extend,count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"NewLine",{
    value:function(count){
      return this.Original.NewLine(count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"NextBookmark",{
    value:function(){
      return this.Original.NextBookmark();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"OpenLink",{
    value:function(){
      return this.Original.OpenLink();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"PageDown",{
    value:function(extend,count){
      return this.Original.PageDown(extend,count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"PageUp",{
    value:function(extend,count){
      return this.Original.PageUp(extend,count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Paste",{
    value:function(){
      return this.Original.Paste();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"PreviousBookmark",{
    value:function(){
      return this.Original.PreviousBookmark();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Replace",{
    value:function(pattern,replace,flags){
      return this.Original.Replace(pattern,replace,flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"SelectAll",{
    value:function(){
      return this.Original.SelectAll();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"SelectLine",{
    value:function(extend){
      return this.Original.SelectLine(extend);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"SetActivePoint",{
    value:function(flags,offset,line,extend){
      return this.Original.SetActivePoint(flags,offset,line,extend);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"SetActivePos",{
    value:function(pos,extend){
      return this.Original.SetActivePos(pos,extend);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"SetAnchorPoint",{
    value:function(flags,offset,line){
      return this.Original.SetAnchorPoint(flags,offset,line);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"SetAnchorPos",{
    value:function(pos){
      return this.Original.SetAnchorPos(pos);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"SetBookmark",{
    value:function(){
      return this.Original.SetBookmark();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"SelectWord",{
    value:function(){
      return this.Original.SelectWord();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"StartOfDocument",{
    value:function(extend){
      return this.Original.StartOfDocument(extend);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"StartOfLine",{
    value:function(extend,flags){
      return this.Original.StartOfLine(extend,flags);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Tabify",{
    value:function(){
      return this.Original.Tabify();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"UnIndent",{
    value:function(){
      return this.Original.UnIndent();
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Untabify",{
    value:function(count){
      return this.Original.Untabify(count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"WordLeft",{
    value:function(extend,count){
      return this.Original.WordLeft(extend,count);
    }
  });
  Object.defineProperty(KL.Selection.prototype,"WordRight",{
    value:function(extend,count){
      return this.Original.WordRight(extend,count);
    }
  });
  if(parseInt(Editor.Version[0])>2){
    Object.defineProperty(KL.Selection.prototype,"AddPoint",{
      value:function(flags,startX,startY,endX,endY){
        return this.Original.AddPoint(flags,startX,startY,endX,endY);
      }
    });
    Object.defineProperty(KL.Selection.prototype,"AddPos",{
      value:function(startPos,endPos){
        return this.Original.AddPos(startPos,endPos);
      }
    });
    Object.defineProperty(KL.Selection.prototype,"Clear",{
      value:function(){
        return this.Original.Clear();
      }
    });
  }
  Object.defineProperty(KL.Selection.prototype,"IsEmpty",{
    get:function(){
      return this.Original.IsEmpty;
    }
  });
  Object.defineProperty(KL.Selection.prototype,"OverwriteMode",{
    get:function(){
      return this.Original.OverwriteMode;
    },
    set:function(value){
      return this.Original.OverwriteMode=value;
    }
  });
  Object.defineProperty(KL.Selection.prototype,"Text",{
    get:function(value){
      return this.Original.Text;
    },
    set:function(value){
      return this.Original.Text=value;
    }
  });
  if(parseInt(Editor.Version[0])>2){
    Object.defineProperty(KL.Selection.prototype,"TextLength",{
      get:function(value){
        return this.Original.TextLength;
      }
    });
  }
  /**
  * 複数の行を選択する。
  * @param {!number} from 開始行。0ならば現在行
  * @param {number} [count=0] 選択行数。0ならば選択しない、負ならば文頭方向へ選択する
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @param {boolean} [exclude=false] trueならば文末側の改行を除外する、falseならば除外しない
  */
  Object.defineProperty(KL.Selection.prototype,"SelectLines",{
    value:function(from,count,mode,exclude){
      if(!count){
        return;
      }
      let active;
      let anchor;
      if(mode==undefined){
        mode=KL.Coord.logical;
      }
      if(mode==KL.Coord.serial){
        if(count<0){
          active=new KL.Point(0,0,mode);
          anchor=new KL.Point(this.OriginalParent.Text.length,0,mode);
        }
        else{
          active=new KL.Point(this.OriginalParent.Text.length,0,mode);
          anchor=new KL.Point(0,0,mode);
        }
      }
      else{
        let lines=this.OriginalParent.GetLines(mode==KL.Coord.view?meGetLineView:meGetLineLogical);
        if(!from){
          from=this.GetActivePointY(mode);
        }
        else{
          from=Math.min(Math.max(from,1),lines);
        }
        let to=Math.min(Math.max((from+count-(count<0?-1:1)),1),lines);
        if(count<0){
          let t=from;
          from=to;
          to=t;
        }
        active=!exclude&&to<lines?new KL.Point(1,to+1,mode):new KL.Point(this.OriginalParent.GetLine(to,mode==KL.Coord.logical?meGetLineLogical:meGetLineView).length+1,to,mode);
        anchor=new KL.Point(1,from,mode);
        if(count<0){
          t=anchor;
          anchor=active;
          active=t;
        }
      }
      this.SetRange(anchor,active);
    }
  });
  /**
  * 複数の行を選択する。
  * @param {!number} [from=0] 開始行。0ならば現在行
  * @param {!number} [to=0] 終了行。0ならば現在行
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @param {boolean} [exclude=false] trueならば文末側の改行を除外する、falseならば除外しない
  */
  Object.defineProperty(KL.Selection.prototype,"SelectLineRange",{
    value:function(from,to,mode,exclude){
      let t=(to||0)-(from||0);
      this.SelectLines(from,t+(t<0?-1:1),mode,exclude);
    }
  });
  /**
  * 選択されていなければ選択する。
  * @param {KL.Coord} [range=KL.Coord.serial] 選択範囲
  */
  Object.defineProperty(KL.Selection.prototype,"SelectIfEmpty",{
    value:function(range){
      if(this.Original.IsEmpty){
        Redraw=false;
        if(!range||range==KL.Coord.serial){
          this.Original.SelectAll();
        }
        else{
          this.Original.StartOfLine(false,range);
          this.Original.EndOfLine(true,range);
          this.Original.CharRight(true);//SelectLineに合わせて"\n"も選択
        }
        Redraw=true;
      }
    }
  });
  /**
  * 論理行頭かどうか判定する。
  * @param {KL.Point|number|undefined} [point=undefined] 座標。numberはKL.Coord.serialとみなす。undefinedのときは現在位置
  * @return {boolean}
  */
  Object.defineProperty(KL.Selection.prototype,"IsLogicalStart",{
    value:function(point){
      if(point==undefined){
        return this.Original.GetActivePointX(mePosLogical)==1;
      }
      if(point instanceof KL.Point&&point.coord==KL.Coord.serial){
        point=point.x;
      }
      if(!isNaN(point)){
        return point==0||this.OriginalParent.Text.charAt(point-1)=="\n";
      }
      if(point.coord==KL.Coord.logical){
        return point.x==1;
      }
      return this.Parent.ViewToLogical(point).x==1;
    }
  });
  /**
  * 論理行末かどうか判定する。
  * @param {KL.Point|number|undefined} [point=undefined] 座標。numberはKL.Coord.serialとみなす。undefinedのときは現在位置
  * @return {boolean}
  */
  Object.defineProperty(KL.Selection.prototype,"IsLogicalEnd",{
    value:function(point){
      if(point==undefined){
        point=this.Original.GetActivePos();
      }
      else if(point instanceof KL.Point){
        if(point.coord==KL.Coord.view){
          point=this.Parent.ViewToSerial(point);
        }
        else if(point.coord==KL.Coord.logical){
          point=this.Parent.LogicalToSerial(point);
        }
        point=point.x;
      }
      return this.OriginalParent.Text.charAt(point)=="\n"||point==this.OriginalParent.Text.length;
    }
  });
  /**
  * 表示行頭かどうか判定する。
  * @param {KL.Point|number|undefined} [point=undefined] 座標。numberはKL.Coord.serialとみなす。undefinedのときは現在位置
  * @return {boolean}
  */
  Object.defineProperty(KL.Selection.prototype,"IsViewStart",{
    value:function(point){
      let r=this.IsLogicalStart(point);
      if(r){
        return r;
      }
      if(point==undefined){
        return this.Original.GetActivePointX(mePosView)==1;
      }
      else if(point instanceof KL.Point){
        if(point.coord==KL.Coord.serial){
          point=this.Parent.SerialToView(point);
          return this.OriginalParent.GetLine(point.y,meGetLineView).length+1==point.x;
        }
        else{
          return point.x==1;
        }
      }
    }
  });
  /**
  * 表示行末かどうか判定する。
  * @param {KL.Point|number|undefined} [point=undefined] 座標。numberはKL.Coord.serialとみなす。undefinedのときは現在位置
  * @return {boolean}
  */
  Object.defineProperty(KL.Selection.prototype,"IsViewEnd",{
    value:function(point){
      if(point==undefined){
        point=this.GetActivePoint(KL.Coord.view);
      }
      else if(point instanceof KL.Point){
        if(point.coord==KL.Coord.serial){
          point=this.Parent.SerialToView(point);
        }
        else if(point.coord==KL.Coord.logical){
          point=this.Parent.LogicalToView(point);
        }
      }
      else if(!isNaN(point)){
        point=this.Parent.SerialToView(point);
      }
      return this.OriginalParent.GetLine(point.y,meGetLineView).length+1==point.x;
    }
  });
  /**
  * 文末かどうか判定する。
  * @param {KL.Point|number|undefined} [point=undefined] 座標。numberはKL.Coord.serialとみなす。undefinedのときは現在位置
  * @return {boolean}
  */
  Object.defineProperty(KL.Selection.prototype,"IsEOF",{
    value:function(point){
      if(point==undefined){
        point=this.Original.GetActivePos();
      }
      else if(point instanceof KL.Point){
        if(point.coord==KL.Coord.view){
          point=this.Parent.ViewToSerial(point).x;
        }
        else if(point.coord==KL.Coord.logical){
          point=this.Parent.LogicalToSerial(point).x;
        }
        else{
          point=point.x;
        }
      }
      return point==this.OriginalParent.Text.length;
    }
  });
  /**
  * 行内選択かどうか判定する。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {boolean}
  */
  Object.defineProperty(KL.Selection.prototype,"Inline",{
    value:function(mode){
      if(mode==undefined){
        mode=KL.Coord.logical;
      }
      if(mode==KL.Coord.serial){
        return true;
      }
      return this.Original.GetActivePointY(mode)==this.Original.GetAnchorPointY(mode);
    }
  });
  /**
  * 複数行に渡る選択かどうか判定する。
  * 最後が改行の場合、{@link KL.Selection.WholeLines}では一行となってもtrueになる。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {boolean}
  */
  Object.defineProperty(KL.Selection.prototype,"Multiline",{
    value:function(mode){
      return !this.Inline(mode);
    }
  });
  /**
  * 行の全体を選択しているかどうか判定する。<br />
  * 行番号クリック時に合わせているので、最後の改行を含まないと全体とは見なさない。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {number} 選択行数。全体を選択していない場合は0
  */
  Object.defineProperty(KL.Selection.prototype,"WholeLines",{
    value:function(mode){
      if(mode==undefined){
        mode=KL.Coord.logical;
      }
      if(mode==KL.Coord.serial){
        return this.OriginalParent.Text.length==this.Original.Text.length?1:0;
      }
      let lines=0;
      let range=this.GetRange(mode);
      if(range.top.x==1){
        let ln=range.bottom.y-range.top.y;
        if(this.Original.Text.slice(-1)=="\n"||this.IsEOF(range.bottom)){
          return ln;
        }
        if(mode==KL.Coord.view){
          if(this.IsViewEnd(range.bottom)&&this.OriginalParent.Text.charAt(this.Parent.ToSerial(range.bottom).x)!="\n"){
            return ln+1;
          }
          if(this.IsViewStart(range.bottom)){
            return ln;
          }
        }
      }
      return lines;
    }
  });
  /**
  * 選択範囲を取得する。
  * @param {KL.Coord} [coord=KL.Coord.serial] 座標系
  * @return {KL.Range}
  */
  Object.defineProperty(KL.Selection.prototype,"GetRange",{
    value:function(coord){
      let range;
      if(!coord){
        range=new KL.Range(new KL.Point(this.Original.GetAnchorPos(),1,KL.Coord.serial),new KL.Point(this.Original.GetActivePos(),1,KL.Coord.serial),this.Parent);
      }
      else{
        range=new KL.Range(this.GetAnchorPoint(coord),this.GetActivePoint(coord),this.Parent);
      }
      return range;
    }
  });
  /**
  * 範囲選択する。
  * @param {KL.Point|number|KL.Range} anchor 開始位置。numberはKL.Coord.serialとみなす。
  *   {@link KL.Range}ならばactiveは不要
  * @param {number|KL.Point} active 終了位置
  */
  Object.defineProperty(KL.Selection.prototype,"SetRange",{
    value:function(anchor,active){
      if(anchor instanceof KL.Range){
        active=anchor.active;
        anchor=anchor.anchor;
      }
      Redraw=false;
      if(!isNaN(anchor)){
        this.Original.SetAnchorPos(anchor);
      }
      else if(anchor.coord==KL.Coord.serial){
        this.Original.SetAnchorPos(anchor.x);
      }
      else{
        this.Original.SetAnchorPoint(anchor.coord,anchor.x,anchor.y);
      }
      if(!isNaN(active)){
        this.Original.SetActivePos(active,true);
      }
      else if(active.coord==KL.Coord.serial){
        this.Original.SetActivePos(active.x,true);
      }
      else{
        this.Original.SetActivePoint(active.coord,active.x,active.y,true);
      }
      Redraw=true;
    }
  });
  /**
  * テキスト選択方向を取得する。
  * @return {KL.Dir} 選択方向
  */
  Object.defineProperty(KL.Selection.prototype,"GetDirection",{
    value:function(){
      return this.Original.IsEmpty?KL.Dir.none:this.Original.GetAnchorPos()>this.Original.GetActivePos()?KL.Dir.backward:KL.Dir.forward;
    }
  });
  /**
  * テキスト選択方向を設定する。
  * @param {KL.Dir} dir 選択方向
  */
  Object.defineProperty(KL.Selection.prototype,"SetDirection",{
    value:function(dir){
      if(dir==KL.Dir.none){
        this.Original.Collapse();
        return;
      }
      if(!this.Original.IsEmpty&&dir!=this.GetDirection()){
        this.Reverse();
      }
    }
  });
  /**
  * 選択範囲を反転する。
  */
  Object.defineProperty(KL.Selection.prototype,"Reverse",{
    value:function(){
      if(!this.Original.IsEmpty){
        this.SetRange(this.Original.GetActivePos(),this.Original.GetAnchorPos());
      }
    }
  });
  /**
  * 選択範囲の文末側の位置を取得する。
  * @return {number}
  */
  Object.defineProperty(KL.Selection.prototype,"GetBottomPos",{
    value:function(){
      return Math.max(this.Original.GetAnchorPos(),this.Original.GetActivePos());
    }
  });
  /**
  * 選択範囲の文頭側の位置を取得する。
  * @return {number}
  */
  Object.defineProperty(KL.Selection.prototype,"GetTopPos",{
    value:function(){
      return Math.min(this.Original.GetAnchorPos(),this.Original.GetActivePos());
    }
  });
  /**
  * カーソル位置を取得する。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Selection.prototype,"GetAnchorPoint",{
    value:function(mode){
      if(mode==undefined){
        mode=KL.Coord.logical;
      }
      if(mode==KL.Coord.serial){
        return new KL.Point(this.Original.GetAnchorPos(),1,mode);
      }
      return new KL.Point(this.Original.GetAnchorPointX(mode),this.Original.GetAnchorPointY(mode),mode);
    }
  });
  /**
  * 選択範囲の開始位置を取得する。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Selection.prototype,"GetActivePoint",{
    value:function(mode){
      if(mode==undefined){
        mode=KL.Coord.logical;
      }
      if(mode==KL.Coord.serial){
        return new KL.Point(this.Original.GetActivePos(),1,mode);
      }
      return new KL.Point(this.Original.GetActivePointX(mode),this.Original.GetActivePointY(mode),mode);
    }
  });
  /**
  * 選択範囲の文末側の位置を取得する。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Selection.prototype,"GetBottomPoint",{
    value:function(mode){
      if(mode==undefined){
        mode=KL.Coord.logical;
      }
      if(mode==KL.Coord.serial){
        return new KL.Point(this.GetBottomPos(),1,mode);
      }
      return new KL.Point(this.Original.GetBottomPointX(mode),this.Original.GetBottomPointY(mode));
    }
  });
  /**
  * 選択範囲の文頭側の位置を取得する。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {KL.Point}
  */
  Object.defineProperty(KL.Selection.prototype,"GetTopPoint",{
    value:function(mode){
      if(mode==undefined){
        mode=KL.Coord.logical;
      }
      if(mode==KL.Coord.serial){
        return new KL.Point(this.GetTopPos(),1,mode);
      }
      return new KL.Point(this.Original.GetTopPointX(mode),this.Original.GetTopPointY(mode));
    }
  });
  /**
  * 選択範囲より左の文字列を取得する。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {string}
  */
  Object.defineProperty(KL.Selection.prototype,"GetLeft",{
    value:function(mode){
      let top=this.GetTopPoint(mode);
      if(mode==KL.Coord.serial){
        return this.OriginalParent.Text.slice(0,top.x);
      }
      return this.OriginalParent.GetLine(top.y,mode==KL.Coord.view?meGetLineView:meGetLineLogical).slice(0,top.x-1);
    }
  });
  /**
  * 選択範囲より右の文字列を取得する。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {string}
  */
  Object.defineProperty(KL.Selection.prototype,"GetRight",{
    value:function(mode){
      let bottom=this.GetBottomPoint(mode);
      if(mode==KL.Coord.serial){
        return this.OriginalParent.Text.slice(bottom.x);
      }
      return this.OriginalParent.GetLine(bottom.y,mode==KL.Coord.view?meGetLineView:meGetLineLogical).slice(bottom.x-1);
    }
  });
  /**
  * 選択部分から文末側の改行を除外する。
  * @param {boolean} [whole=false] 行全体を選択している場合のみ除外する
  */
  Object.defineProperty(KL.Selection.prototype,"Exclude",{
    value:function(whole){
      if(this.Original.IsEmpty||whole&&this.WholeLines(KL.Coord.logical)==0){
        return;
      }
      let range=this.GetRange(KL.Coord.serial);
      if(range.direction&&this.IsLogicalStart(range.bottom)){
        range.direction==KL.Dir.forward?range.active.x-=1:range.anchor.x-=1;
      }
      this.SetRange(range);
    }
  });
  /**
  * テキスト二重化。
  * @param {KL.Coord|undefined} [mode=undefined] 行モード。undefinedの場合、選択されていれば選択部分、さもなくば論理行を二重化する
  */
  Object.defineProperty(KL.Selection.prototype,"Duplicate",{
    value:function(mode){
      let range=this.GetRange(KL.Coord.serial);
      let scroll=KL.GetScroll();
      if(mode==undefined){
        if(this.IsEmpty){
          mode=KL.Coord.logical;
        }
        else{
          this.Original.Text+=this.Original.Text;
        }
      }
      if(mode!=undefined){
        let r=this.GetRange(mode);
        this.SelectLineRange(r.top.y,r.bottom.y,mode,false);
      }
      this.Original.Text+=this.Original.Text;
      this.SetRange(range);
      KL.SetScroll(scroll);
    }
  });
  /**
  * 選択範囲の行または文字列をソートする。
  * @param {KL.Coord|string} [mode=KL.Coord.logical] 行モード。未選択だと文書全体を対象とする。
  *   KL.Coord.logical、KL.Coord.viewなら行単位でソートする。KL.Coord.serialは選択文字列をソートする。
  *   文字列を指定するとKL.Coord.serial扱いとなり、その文字列を区切りとしてソートする。
  * @param {Function} [func=null] 比較関数。
  */
  Object.defineProperty(KL.Selection.prototype,"Sort",{
    value:function(mode,func){
      let arr=[];
      let delim="";
      if(typeof(mode)=="string"){
        delim=mode;
        mode=KL.Coord.serial;
      }
      this.SelectIfEmpty();
      if(mode==KL.Coord.serial){
        arr=this.Original.Text.split(delim);
        arr=arr.sort(func);
        this.Original.Text=arr.join(delim);
      }
      else{
        let r=this.GetRange(mode);
        this.SelectLineRange(r.top.y,r.bottom.y,mode,false);
        let flag=mode==KL.Coord.logical?meGetLineLogical:meGetLineView;
        let total=this.Original.GetAnchorPos();
        let text=this.OriginalParent.Text;
        for(let i=r.top.y;i<=r.bottom.y;i++){
          let line=this.OriginalParent.GetLine(i,flag);
          total+=line.length;
          if(text.charAt(total)=="\n"){
            line+="\n";
            total+=1;
          }
          arr.push(line);
        }
        arr=arr.sort(func);
        this.Original.Text=arr.join("");
      }
    }
  });
  /**
  * 辞書昇順でソートする。
  */
  Object.defineProperty(KL.Selection.prototype.Sort,"StrAsc",{
    value:function(a,b){
      return a<b?-1:a>b?1:0;
    }
  });
  /**
  * 辞書降順でソートする。
  */
  Object.defineProperty(KL.Selection.prototype.Sort,"StrDesc",{
    value:function(a,b){
      return a<b?1:a>b?-1:0;
    }
  });
  /**
  * ケース無視で辞書昇順でソートする。
  */
  Object.defineProperty(KL.Selection.prototype.Sort,"InsAsc",{
    value:function(a,b){
      let aa=a.toLowerCase();
      let bb=b.toLowerCase();
      return aa<bb?-1:aa>bb?1:0;
    }
  });
  /**
  * ケース無視で辞書逆順でソートする。
  */
  Object.defineProperty(KL.Selection.prototype.Sort,"InsDesc",{
    value:function(a,b){
      let aa=a.toLowerCase();
      let bb=b.toLowerCase();
      return aa<bb?1:aa>bb?-1:0;
    }
  });
  /**
  * 長さ昇順でソートする。
  */
  Object.defineProperty(KL.Selection.prototype.Sort,"LenAsc",{
    value:function(a,b){
      return a.length-b.length;
    }
  });
  /**
  * 長さ降順でソートする。
  */
  Object.defineProperty(KL.Selection.prototype.Sort,"LenDesc",{
    value:function(a,b){
      return b.length-a.length;
    }
  });
  /**
  * 逆順にソートする。
  */
  Object.defineProperty(KL.Selection.prototype.Sort,"Rev",{
    value:function(a,b){
      return -1;
    }
  });
  /**
  * 行を削除する。
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @param {function(number,string):boolean|number|Array.<number>} selector 選択関数もしくは行番号（の配列）
  */
  Object.defineProperty(KL.Selection.prototype,"DeleteLines",{
    value:function(mode,selector){
      let lines=this.Parent.Text.split("\n");
      if(selector instanceof Function){
        lines=lines.filter(function(){return !selector.apply(this,arguments)});
      }
      else if(selector instanceof Array){
        lines=lines.filter(function(n,s){return !(n in selector);});
      }
      else if(!isNaN(selector)){
        lines.splice(selector,1);
      }
      this.Parent.Text=lines.join(this.Parent.LineEndingChar);
    }
  });
  /**
  * スクロールバーの位置を取得する。
  * @return {KL.Point}
  */
  KL.GetScroll=function(){
    return new KL.Point(ScrollX,ScrollY);
  }
  /**
  * スクロールバーの位置を設定する。
  * @param {number|KL.Point} x 横スクロールバーの位置または両スクロールバーの位置
  * @param {number} y 縦スクロールバーの位置
  */
  KL.SetScroll=function(x,y){
    if(x instanceof KL.Point){
      y=x.y;
      x=x.x;
    }
    Redraw=false;
    ScrollX=x;
    ScrollY=y;
    Redraw=true;
  }
}
