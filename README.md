# About

テキストエディタ[Mery](http://www.haijin-boys.com/wiki)の用ライブラリです。
ChakraとKLライブラリの使用を前提として作り直しています。

# Contents

- KL

コアライブラリです。ChakraとMery組み込みオブジェクトへの補完/追加を行います。

- Exes

実行用マクロです。大抵のマクロは直接呼び出す様にはできていないので、Meryからはこのディレクトリのファイルを選択します。

# Lisence

MIT（旧バージョンのNYSLから変更していますが、実際のところどちらでも構いません）

Copyright 2019 黄身

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
