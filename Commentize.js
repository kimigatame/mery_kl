#title="コメント切り替え"
#include "KL/KL.js"

/**
* コメント化もしくはコメント解除する。
* @class
* @param {string} path 編集モード設定ファイルのパス
* @param {?number} [clear=0] -1ならトグル、0ならコメント化、1ならコメント解除、2なら1行目で判定する
* @param {?boolean} [after=false] コメント化の時、インデントの後に挿入する
* @param {?string} [mode="DEFAULT"] 編集モード
*/
let Commentize=function(path,clear,after,mode){
  let $d=new KL.Document();
  let $s=$d.Selection;
  /**
  * 編集モードの設定のリスト。<br />
  * モード名は大文字にすること。
  * @typedef {object.<string,string>} モード名と行コメント開始文字列のペア
  */
  let modes;
  let file=new ActiveXObject("Scripting.FileSystemObject").OpenTextFile(path,1,true,-1);
  if(!file.AtEndOfStream){
    modes=JSON.parse(file.ReadAll());
  }
  file.Close();
  mode=(mode||$d.Mode).toUpperCase();
  let comment=mode in modes?modes[mode]:modes.DEFAULT;
  if(comment){
    $s.Exclude();
    let range=$s.GetRange(KL.Coord.logical);
    $s.SelectLineRange(range.top.y,range.bottom.y,KL.Coord.logical,true);
    let seg=$s.Text;
    let pat=new RegExp("^(\\s*)"+comment);
    let clearComment=function(seg){
      return seg.replace(new RegExp("^(\\s*)"+comment,"gm"),"$1");
    }
    let insertComment=function(seg){
      if(after){
        let pat=/^\s*/gm;
        let indent=null;
        while(pat.test(seg)){
          if(indent==null||RegExp.lastMatch.length<indent.length){
            indent=RegExp.lastMatch;
            if(indent.length==0){
              break;
            }
          }
        }
        seg=seg.replace(new RegExp("^"+indent,"gm"),indent+comment);
      }
      else{
        seg=seg.replace(/^/gm,comment);
      }
      return seg;
    }
    if(clear==2){
      clear=pat.test(seg);
    }
    if(clear<0){
      seg=seg.split("\n");
      let t=[];
      let result=[];
      let status=null;//1:コメント、2:通常
      for(let i=0;i<seg.length;i++){
        if(pat.test(seg[i])){
          if(status==1){
            result.push(insertComment(t.join("\n")));
            t=[];
          }
          t.push(seg[i]);
          status=2;
        }
        else{
          if(status==2){
            result.push(clearComment(t.join("\n")));
            t=[];
          }
          t.push(seg[i]);
          status=1;
        }
      }
      result.push(status==1?insertComment(t.join("\n")):clearComment(t.join("\n")));
      seg=result.join("\n");
    }
    else{
      seg=clear?clearComment(seg):insertComment(seg);
    }
    $s.Text=seg;
  }
}
