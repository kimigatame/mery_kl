#title="テンプレート挿入"
#include "KL/all.js"

/**
* キャレット行の情報からテンプレートを検索する。<br />
* リストファイルはJSONで、形式はComText.Settings。サンプルファイルを参照。<br />
* 使用の流れは以下の通り。
* <ol>
* <li>ComText#matchを実行することで、</li>
* <li>編集モードがComText.Entry#modesにマッチするエントリーを選択し、</li>
* <li>キャレット行をそれぞれのComText.Entry#conditionsに基づいて検索し、</li>
* <li>全てマッチした場合ComText.Optionを作成し、</li>
* <li>ComText#optionsに格納する。</li>
* </ol>
* マッチするものが無かった場合はComText.Settings#subがあればComText#optionsに格納する。<br />
* それも無い場合は空の配列になる。<br />
* 取得したテンプレートの利用はComText.Optionに依存する。
* @class
* @param {string} path リストファイルのパス
* @param {ComText.Option} option マッチ結果を格納するComText.Option（を継承したオブジェクト）
*/
function ComText(path,option){
  /**
  * リスト本体。
  * @type {Array.<ComText.Entry>}
  */
  this.entries=[];
  /**
  * 代替テンプレート。
  * @type {Array.<ComText.Entry>}
  */
  this.sub;
  /**
  * マッチ結果のリスト。
  * @type {Array.<ComText.Option>}
  */
  this.options=[];
  /**
  * キャレット行の情報。
  * @type {ComText.Line}
  */
  this.line;
  /**
  * マッチ結果を保存するオブジェクト。
  * @type {Array.<ComText.Option>}
  */
  this.option=option&&new option() instanceof ComText.Option?option:ComText.Option;
  //constructor
  if(path){
    this.readList(path);
  }
}
/**
* リストファイルを読み込む。
* @param {string} path リストファイルのパス
*/
ComText.prototype.readList=function(path){
  let file=new ActiveXObject("Scripting.FileSystemObject").OpenTextFile(path,1,true,-1);
  if(!file.AtEndOfStream){
    let mode=GDO().Mode;
    let t=JSON.parse(file.ReadAll());
    this.entries=t.entries.filter(function(value,index){return !value.modes||!value.modes.length||value.modes.indexOf(mode)>-1;});
    this.sub=t.sub;
  }
  file.Close();
}
/**
* キャレット行の情報を解析する。
* @param {KL.Coord} [mode=KL.Coord.logical] 行モード
* @return {ComText.Line}
*/
ComText.prototype.parseLine=function(mode){
  if(mode==undefined){
    mode=KL.Coord.logical;
  }
  let $s=new KL.Selection();
  this.line=new ComText.Line();
  let t=$s.GetLeft(mode);
  this.line.text=t;
  this.line.basePos=$s.GetTopPos()-t.length;
  this.line.noblank.start=/(?=[^\s]|$)/.test(t)?RegExp.leftContext.length:0;
  this.line.selection.start=this.line.text.length;
  t=$s.Text;
  this.line.text+=t;
  this.line.selection.end=this.line.text.length;
  this.line.text+=$s.GetRight(mode);
  this.line.full.end=this.line.text.length;
  this.line.noblank.end=/[^\s](\s*)$/.test(this.line.text)?this.line.full.end-RegExp.$1.length:this.line.full.end;
}
/**
* 行情報を使いエントリーのリストからマッチするものを取得する。
  * @param {bool} [shortcut=false] 一つのエントリーがマッチした時点で終了する
  * @param {KL.Coord} [mode=KL.Coord.logical] 行モード
  * @return {ComText} 自身
*/
ComText.prototype.match=function(shortcut,mode){
  /**
  * マッチ範囲を表すオブジェクトを作成する。
  * @param {boolean} positive 範囲を設定する必要がある
  * @param {number} [start] 開始位置
  * @param {number} [end] 終了位置
  * @return {ComText.Range}
  */
  function createMatch(positive,start,end){
    if(positive){
      return new ComText.Range(start,end);
    }
    else{
      return new ComText.Range();//否定検索の場合範囲は存在しない
    }
  }
  this.parseLine(mode);
  for(let i=0;i<this.entries.length;i++){
    let matchRanges=[];//conditionにマッチした範囲
    if(this.entries[i].template===null){
      this.options.push(new this.option(this.entries[i],null,null));
      continue;
    }
    for(let j=0;j<this.entries[i].conditions.length;j++){
      let text=this.entries[i].conditions[j].text;
      let space=this.entries[i].conditions[j].spaces;
      let positive=this.entries[i].conditions[j].type>0;//肯定検索
      let type=Math.abs(this.entries[i].conditions[j].type);
      let subtext="";//テキストの内マッチに必要な部分
      switch(type){
        case ComText.SearchTypes.head:
        case ComText.SearchTypes.selectionFront:
        case ComText.SearchTypes.beforeSelection:
        case ComText.SearchTypes.beforeLength:
        case ComText.SearchTypes.beforeContain:
        case ComText.SearchTypes.beforeRegExp:
          subtext=this.line.text.substring(space?0:this.line.noblank.start,this.line.selection.start);
          break;
        case ComText.SearchTypes.tail:
        case ComText.SearchTypes.selectionBack:
        case ComText.SearchTypes.afterSelection:
        case ComText.SearchTypes.afterLength:
        case ComText.SearchTypes.afterContain:
        case ComText.SearchTypes.afterRegExp:
          subtext=this.line.text.substring(this.line.selection.end,space?this.line.full.end:this.line.noblank.end);
          break;
        case ComText.SearchTypes.selection:
        case ComText.SearchTypes.selectionLength:
        case ComText.SearchTypes.selectionContain:
        case ComText.SearchTypes.selectionRegExp:
          subtext=this.line.text.substring(this.line.selection.start,this.line.selection.end);
          break;
        case ComText.SearchTypes.this.lineLength:
        case ComText.SearchTypes.this.lineContain:
        case ComText.SearchTypes.this.lineRegExp:
          subtext=space?this.line.text:this.line.text.substring(this.line.noblank.start,this.line.noblank.end);
          break;
        default:
          break;
      }
      //textがconditionを満たす場合マッチ範囲を取得
      let t;
      switch(type){
        case ComText.SearchTypes.head:
          if(subtext.startsWith(text)==positive){
            t=space?0:this.line.noblank.start;
            matchRanges.push(createMatch(positive,t,t+text.length));
            continue;
          }
          break;
        case ComText.SearchTypes.tail:
          if(subtext.endsWith(text)==positive){
            t=this.line.full.end-(space?0:this.line.full.end-this.line.noblank.end);
            matchRanges.push(createMatch(positive,t-text.length,t));
            continue;
          }
          break;
        case ComText.SearchTypes.selectionFront:
          if(subtext.endsWith(text)==positive){
            matchRanges.push(createMatch(positive,this.line.selection.start-text.length,this.line.selection.start));
            continue;
          }
          break;
        case ComText.SearchTypes.selectionBack:
          if(subtext.startsWith(text)==positive){
            matchRanges.push(createMatch(positive,this.line.selection.end,this.line.selection.end+text.length));
            continue;
          }
          break;
        case ComText.SearchTypes.beforeSelection:
          if((subtext==text)==positive){
            matchRanges.push(createMatch(positive,this.line.selection.start-text.length,this.line.selection.start));
            continue;
          }
          break;
        case ComText.SearchTypes.afterSelection:
          if((subtext==text)==positive){
            matchRanges.push(createMatch(positive,this.line.selection.end,this.line.selection.end+text.length));
            continue;
          }
          break;
        case ComText.SearchTypes.selection:
          if((subtext==text)==positive){
            matchRanges.push(createMatch(positive,this.line.selection.start,this.line.selection.end));
            continue;
          }
          break;
        case ComText.SearchTypes.beforeLength:
          if((subtext.length==text)==positive){
            matchRanges.push(createMatch(positive,this.line.selection.start-text.length,this.line.selection.start));
            continue;
          }
          break;
        case ComText.SearchTypes.afterLength:
          if((subtext.length==text)==positive){
            matchRanges.push(createMatch(positive,this.line.selection.end,this.line.selection.end+text.length));
            continue;
          }
          break;
        case ComText.SearchTypes.selectionLength:
          if((subtext.length==text)==positive){
            matchRanges.push(createMatch(positive,this.line.selection.start,this.line.selection.end));
            continue;
          }
          break;
        case ComText.SearchTypes.beforeContain:
          t=subtext.indexOf(text);
          if((t>-1)==positive){
            t=space?t:t+this.line.noblank.start;
            matchRanges.push(createMatch(positive,t,t+text.length));
            continue;
          }
          break;
        case ComText.SearchTypes.afterContain:
          t=subtext.indexOf(text);
          if((t>-1)==positive){
            t=this.line.selection.end+t;
            matchRanges.push(createMatch(positive,t,t+text.length));
            continue;
          }
          break;
        case ComText.SearchTypes.selectionContain:
          t=subtext.indexOf(text);
          if((t>-1)==positive){
            t=this.line.selection.start+t;
            matchRanges.push(createMatch(positive,t,t+text.length));
            continue;
          }
          break;
        case ComText.SearchTypes.beforeRegExp:
          if(new RegExp(text).test(subtext)==positive){
            t=space?RegExp.index:RegExp.index+this.line.noblank.start;
            matchRanges.push(createMatch(positive,t,t+RegExp.lastMatch.length));
            continue;
          }
          break;
        case ComText.SearchTypes.afterRegExp:
          if(new RegExp(text).test(subtext)==positive){
            matchRanges.push(createMatch(positive,RegExp.index+this.line.selection.end,RegExp.lastIndex+this.line.selection.end));
            continue;
          }
          break;
        case ComText.SearchTypes.selectionRegExp:
          if(new RegExp(text).test(subtext)==positive){
            t=RegExp.lastIndex+this.line.selection.start;
            matchRanges.push(createMatch(positive,t,t+RegExp.lastMatch.length));
            continue;
          }
          break;
        case ComText.SearchTypes.this.lineLength:
          if((subtext.length==text)==positive){
            t=space?0:this.line.noblank.start;
            matchRanges.push(createMatch(positive,t,t+text.length));
          }
          break;
        case ComText.SearchTypes.this.lineContain:
          t=subtext.indexOf(text);
          if((t>-1)==positive){
            t=space?t:this.line.noblank.start+t;
            matchRanges.push(createMatch(positive,t,t+text.length));
          }
          break;
        case ComText.SearchTypes.this.lineRegExp:
          if(new RegExp(text).test(subtext)==positive){
            t=space?RegExp.index:RegExp.lastIndex+this.line.noblank.start;
            matchRanges.push(createMatch(positive,t,t+RegExp.lastMatch.length));
            continue;
          }
          break;
        default:
          break;
      }
    }
    if(matchRanges.length!=this.entries[i].conditions.length){
      continue;//マッチしないconditionがあるので失敗。このエントリーは終わり
    }
    this.options.push(new this.option(this.entries[i],this.line,matchRanges));
    if(shortcut){
      break;
    }
  }
  if(!this.options.length&&this.sub){
    this.options.push(new this.option(this.sub,this.line,createMatch(false)));
  }
  else{
    this.options=this.options.sort(function(a,b){return (b.entry.weight||0)-(a.entry.weight||0);});
  }
  
  return this;
}

/**
* キャレット行の情報。<br />
* basePosを除き、位置はComText.Line#textにおけるインデックス。
* @class
*/
ComText.Line=function(){
  /**
  * 選択行全体。
  * @type {string}
  */
  this.text=undefined;
  /**
  * 対象範囲の先頭からの位置。
  * @type {number}
  */
  this.basePos=0;
  /**
  * 前後の空白を除いた位置。
  * @type {ComText.Range}
  */
  this.noblank=new ComText.Range();
  /**
  * 選択部分の位置。
  * @type {ComText.Range}
  */
  this.selection=new ComText.Range();
  /**
  * 先頭と末尾の位置。startは常に0、endは常にComText.Line#textの長さに等しい。
  * @type {ComText.Range}
  */
  this.full=new ComText.Range();
}
/**
* 範囲を表す。
* @class
* @param {number} [start=0] 開始位置
* @param {number} [end=0] 終了位置
*/
ComText.Range=function(start,end){
  /**
  * 開始位置。
  * @type {number}
  */
  this.start=start||0;
  /**
  * 終了位置。
  * @type {number}
  */
  this.end=end||0;
  /**
  * 配列として返す。
  * @type {number}
  * @return {Array.<number>}
  */
  this.toArray=function(){
    return [this.start,this.end];
  }
  /**
  * 長さを返す。
  * @type {number}
  * @return {number}
  */
  this.getLength=function(){
    return Math.abs(this.start-this.end);
  }
}
/**
* 範囲を表す文字列を解析する
* @param {string} str 範囲を表す文字列
* @param {ComText.Line} line 検索行
* @param {Array.<ComText.Range>} matchRanges マッチ結果
* @return {ComText.Range}
*/
ComText.parseRangeField=function(str,line,matchRanges){
  let pattern=/^(\d+)$|^(?:(\d+):)?(?:(\d)([+-]\d+)?),(?:(\d+):)?(?:(\d)([+-]\d+)?)$/g;
  if(!str.match(pattern)){
    throw new Error(0,"range parse failed : "+str);
  }
  let cap1;
  let cap2;
  let pos1;
  let pos2;
  let delta1=0;
  let delta2=0;
  if(RegExp.$1){
    cap1=RegExp.$1;
    cap2=RegExp.$1;
    pos1=ComText.RangeTypes.matchStart;
    pos2=ComText.RangeTypes.matchEnd;
  }
  else{
    cap1=RegExp.$2||0;
    cap2=RegExp.$5||0;
    pos1=RegExp.$3;
    pos2=RegExp.$6;
    delta1=parseInt(RegExp.$4)||0;
    delta2=parseInt(RegExp.$7)||0;
  }
  let t=[parseInt(pos1),parseInt(pos2||ComText.RangeTypes.matchEnd)];
  for(let i=0;i<t.length;i++){
    switch(t[i]){
      case ComText.RangeTypes.noblankStart:
        t[i]=line.noblank.start;
        break;
      case ComText.RangeTypes.noblankEnd:
        t[i]=line.noblank.end;
        break;
      case ComText.RangeTypes.selectionStart:
        t[i]=line.selection.start;
        break;
      case ComText.RangeTypes.selectionEnd:
        t[i]=line.selection.end;
        break;
      case ComText.RangeTypes.fullStart:
        t[i]=line.full.start;
        break;
      case ComText.RangeTypes.fullEnd:
        t[i]=line.full.end;
        break;
      case ComText.RangeTypes.matchStart:
        t[i]=matchRanges[i==1&&cap2?cap2:cap1||0].start;
        break;
      case ComText.RangeTypes.matchEnd:
        t[i]=matchRanges[i==1&&cap2?cap2:cap1|0].end;
        break;
      default:
        t[i]=i==0?matchRanges[cap1||0].start:matchRanges[cap2||cap1||0].end;
        break;
    }
  }
  return new ComText.Range(Math.clamp((t[0]+delta1),line.full.start,line.full.end),Math.clamp((t[1]+delta2),line.full.start,line.full.end));
}
/**
* 行情報にマッチしたエントリーと関連情報。<br />
* デフォルトでは文字列の作成までしか行わないので、その後の処理の為にはComText.Optionを継承して拡張するか、別の関数に渡す必要がある。
* @example
* let InheritedOption=function(){
*   ComText.Option.apply(this,arguments);
* }
* Object.setPrototypeOf(InheritedOption.prototype,ComText.Option.prototype);
* InheritedOption.prototype.constructor=InheritedOption;
* InheritedOption.prototype.appendix=function(txt){}//オーバーライド
* @class
* @param {ComText.Entry} entry 対応するエントリー
* @param {ComText.Line} line 検索行の情報
* @param {Array.<ComText.Range>} matchRanges マッチ結果
*/
ComText.Option=function(entry,line,matchRanges){
  if(!arguments.length){
    return;
  }
  /**
  * 名前。
  * @type {string}
  */
  this.name=entry.name;
  /**
  * 対応するエントリー。
  * @type {ComText.Entry}
  */
  this.entry=entry;
  let txt=undefined;
  /**
  * 生成されたテキスト。
  * @type {string}
  */
  Object.defineProperty(this,"text",{
    get:function(){
      if(txt){
        return txt;
      }
      let pattern=/\{(\d+|(?:\d+:)?\d(?:[+-]\d+)?,(?:\d+:)?\d(?:[+-]\d+)?)\}/g;
      txt=this.entry.template.replace(pattern,function($1,$2){
        let range=ComText.parseRangeField($2,line,matchRanges);
        return line.text.substring(range.start,range.end);
      });
      if(this.entry.indent){
        txt=txt.split("\n").join("\n"+this.line.text.substring(this.line.full.start,this.line.noblank.start));
      }
      txt=txt.split("{{}").join("{").split("{}}").join("}");
      txt=this.appendix(txt);
      return txt;
    }
  });
  /**
  * 検索行の情報。
  * @type {ComText.Line}
  */
  this.line=line;
  /**
  * マッチ結果。否定検索の場合、全ての値は常に0。
  * @type {Array.<ComText.Range>}
  */
  this.matchRanges=matchRanges;
}
/**
* 追加のフォーマット処理。オーバーライドすると文字列の埋め込み後、「{}」のアンエスケープの前に実行される。
* @param {string} str
* @return {string}
*/
ComText.Option.prototype.appendix=function(str){
  return str;
};
/**
* テンプレートJSONファイルの形式。
* @class
*/
ComText.Settings=function(){
  /**
  * テンプレートリスト。
  * @type {Array.<ComText.Entry>}
  */
  this.entries=undefined;
  /**
  * 全てのエントリーがマッチしなかった場合に使用される代替テンプレート。<br />
  * ComText.Entry#conditionsは一切考慮されない。
  * @type {ComText.Entry}
  */
  this.sub=undefined;
}
/**
* テンプレートと検索条件。
* @class
*/
ComText.Entry=function(){
  /**
  * 名前。
  * @type {string}
  */
  this.name=undefined;
  /**
  * テンプレート。<br />
  * ComText.Entry#rangeと同じ文字列を「{}」で括って埋め込むと、その範囲の文字列で置換される。<br />
  * 「{s}」「{e}」はそれぞれ挿入後の選択開始位置、選択終了位置を表す。一方のみだとその位置にキャレットを移動する。
  * 置換パターンは「{{}」「{}}」でエスケープできる。
  * @type {string}
  */
  this.template=undefined;
  /**
  * 複数行に渡る場合、インデントを1行目に揃える。
  * @type {boolean}
  */
  this.indent=undefined;
  /**
  * 検索条件。
  * @type {Array.<ComText.Condition>}
  */
  this.conditions=undefined;
  /**
  * 挿入位置の範囲設定。<br />
  * 「index」または「[index:]startPos[delta],[index:]endPos[delta]」の形式で検索によって得られたComText.RangeとComText.Lineに基づいて範囲を指定する。<br />
  * indexはComText.Generated#matchesのインデックスを表す。<br />
  * startPosとendPosはComText.RangeTypesを指定する事で、その範囲の文字列に置換される。<br />
  * deltaはそれぞれの位置からの差分を表す。但しComText.Line#fullに挿入したテキストの長さを加えた範囲に丸められる。<br/>
  * indexのみ指定した場合は「index:7,index:8」と同じ。<br />
  * インデックスを省略した場合は「0」とみなす。<br />
  * nullと解される場合は「3,4」とみなす。
  * @type {string}
  */
  this.range=undefined;
  /**
  * 適用する編集モード。指定しなければ全モードで使用される。
  * @type {Array.<string>}
  */
  this.modes=undefined;
  /**
  * 優先度。大きい程ComText#generatedの先頭に来る。
  * @type {number}
  */
  this.weight=undefined;
}
/**
* 検索条件。
* @class
*/
ComText.Condition=function(){
  /**
  * 検索タイプ。ComText.SearchTypesを用いるが負の値にすると否定検索となる
  * @type {number}
  */
  this.type=undefined;
  /**
  * 検索文字列。
  * @type {string|number}
  */
  this.text=undefined;
  /**
  * 検索対象に行頭行末の空白を含める。
  * @type {bool}
  */
  this.spaces=undefined;
}
/**
* 範囲指定に用いる座標。
* @enum {number}
*/
ComText.RangeTypes={
  /**
  * 空白を除いた先頭位置。
  */
  noblankStart:1,
  /**
  * 空白を除いた末尾位置。
  */
  noblankEnd:2,
  /**
  * 選択開始位置。
  */
  selectionStart:3,
  /**
  * 選択終了位置。
  */
  selectionEnd:4,
  /**
  * 先頭位置。
  */
  fullStart:5,
  /**
  * 末尾位置。
  */
  fullEnd:6,
  /**
  * マッチ結果の開始位置。
  */
  matchStart:7,
  /**
  * マッチ結果の終了位置。
  */
  matchEnd:8
}
/**
* 検索タイプ。
* @enum {number}
*/
ComText.SearchTypes={
  /**
  * 先頭にマッチする。
  */
  head:1,
  /**
  * 末尾にマッチする。
  */
  tail:2,
  /**
  * 選択位置の直前にマッチする。
  */
  selectionFront:3,
  /**
  * 選択位置の直後にマッチする。
  */
  selectionBack:4,
  /**
  * 選択位置より前の文字列に一致する。
  */
  beforeSelection:5,
  /**
  * 選択位置より後の文字列に一致する。
  */
  afterSelection:6,
  /**
  * 選択文字列に一致する。
  */
  selection:7,
  /**
  * 選択位置より前の文字列の長さが一致する。
  */
  beforeLength:8,
  /**
  * 選択位置より後の文字列の長さが一致する。
  */
  afterLength:9,
  /**
  * 選択文字列の長さが一致する。
  */
  selectionLength:10,
  /**
  * 選択位置より前の文字列に含まれる。
  */
  beforeContain:11,
  /**
  * 選択位置より後の文字列に含まれる。
  */
  afterContain:12,
  /**
  * 選択文字列に含まれる。
  */
  selectionContain:13,
  /**
  * 選択位置より前の文字列を正規表現検索する。
  */
  beforeRegExp:14,
  /**
  * 選択位置より後の文字列を正規表現検索する。
  */
  afterRegExp:15,
  /**
  * 選択文字列を正規表現検索する。
  */
  selectionRegExp:16,
  /**
  * 行全体の長さが一致する。
  */
  lineLength:17,
  /**
  * 行全体に含まれる。
  */
  lineContain:18,
  /**
  * 行全体を正規表現検索する。
  */
  lineRegExp:19
}
