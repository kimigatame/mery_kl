#include "ComText.js"
#include "WidePop.js"

/**
* 選択部分をテンプレートにはめ込んで作成した文字列で検索する。<br />
* 「<##>」が選択文字列に置き換えられる（{@link InsetSearch~here}で変更可能）。<br />
* リストファイルはJSONで、形式は「"entries":[InsetSearch.Entry...]」。サンプルファイルを参照。
* @class
* @param {string} path リストファイルのパス
*/
function InsetSearch(path){
  ComText.call(this,path);
}
Object.setPrototypeOf(InsetSearch.prototype,ComText.prototype);
InsetSearch.prototype.constructor=InsetSearch;
/**
* 検索を実行する。
*/
InsetSearch.prototype.execute=function(){
  /**
  * 検索本体。
  * @param {string} template 正規表現
  * @param {?number} flag Selection.Findで使うフラグ
  */
  let search=function(str,flag){
    let $s=Editor.ActiveDocument.Selection;
    if(flag==null){
      flag=meFindNext|meFindReplaceCase;
    }
    $s.Find(str,flag);
  };
  let menu=[];
  this.match();
  for(let i=0;i<this.options.length;i++){
    if(!this.options[i].entry.template){
      if(menu.length&&menu.last.length>0&&i<this.options.length-1){
        menu.push([]);
      }
    }
    else{
      menu.push([this.options[i].name,1,function(pop,me){search(me.tags[0],me.tags[1]);},null,[this.options[i].text,this.options[i].entry.flags]]);
    }
  }
  new WidePop(menu).create().track(1,false);
};

/**
* 検索用テンプレート。
* @class
*/
InsetSearch.Entry=function(){
  ComText.Entry.call(this);
  /**
  * 検索フラグ。
  * @type {number}
  * <table>
  * <tr><td>0</td><td>前を検索</td></tr>
  * <tr><td>1</td><td>次を検索</td></tr>
  * <tr><td>2</td><td>大小文字を区別する</td></tr>
  * <tr><td>4</td><td>単語のみ検索する</td></tr>
  * <tr><td>8</td><td>文末まで検索したら文頭へ移動する</td></tr>
  * <tr><td>16</td><td>正規表現で検索する</td></tr>
  * </table>
  */
  this.flags;
}
