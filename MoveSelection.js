#title="選択部分を移動"
#include "KL/KL.js"

/**
* 選択部分を移動する。
* @class
* @param {number} dir 移動方向。4:左、6:右、8:上、2:下
* @param {KL.Coord} [coord=KL.Coord.serial] 座標系
* @param {number} [delta=1] 移動量
*/
let MoveSelection=function(dir,coord,delta){
  if(coord==undefined){
    coord=KL.Coord.logical;
  }
  if(!delta){
    delta=1;
  }
  let $d=new KL.Document();
  let $s=$d.Selection;
  let destRange;
  let range=$s.GetRange(coord);
  let selLength=$s.Text.length;
  if(range.direction==KL.Dir.none){
    return;
  }
  switch(dir){
    case 4:
      if(range.top.x==0){
        break;
      }
      range.top.x=Math.max(range.top.x-delta,0);
      $s.SetRange(range);
      $s.Text=$s.Text.slice(-selLength)+$s.Text.slice(0,-selLength);
      destRange=range;
      destRange.bottom.x=destRange.top.x+selLength;
      if(destRange.direction==KL.Dir.backward){
        destRange.reverse();
      }
      $s.SetRange(destRange);
      break;
    case 6:
      let limit=0;
      if(coord==KL.Coord.serial){
        limit=$d.Text.length;
      }
      else{
        limit=$d.GetLine(range.active.y,coord==KL.Coord.logical?meGetLineLogical:meGetLineView).length+1;
      }
      if(range.bottom.x==limit){
        break;
      }
      range.bottom.x=Math.min(range.bottom.x+delta,limit);
      $s.SetRange(range);
      $s.Text=$s.Text.slice(selLength)+$s.Text.slice(0,selLength);
      destRange=range;
      destRange.top.x=destRange.bottom.x-selLength;
      if(destRange.direction==KL.Dir.backward){
        destRange.reverse();
      }
      $s.SetRange(destRange);
      break;
    case 8:
      if(coord==KL.Coord.serial||range.top.y==1){
        break;
      }
      range.top.y=Math.max(range.top.y-delta,1);
      $s.SetRange(range);
      let str=$s.Text.slice(-selLength)+$s.Text.slice(0,-selLength);
      $s.Text=str;
      destRange=$s.GetRange(KL.Coord.serial);
      destRange.top.x-=str.length;
      destRange.bottom.x=destRange.top.x+selLength;
      if(range.direction==KL.Dir.backward){
        destRange.reverse();
      }
      $s.SetRange(destRange);
      break;
    case 2:
      if(coord==KL.Coord.serial||range.bottom.y==$d.GetLines(coord==KL.Coord.logical?meGetLineLogical:meGetLineView)){
        break;
      }
      range.bottom.y=Math.min(range.bottom.y+delta,$d.GetLines(coord==KL.Coord.logical?meGetLineLogical:meGetLineView));
      range.bottom.x-=selLength;
      $s.SetRange(range);
      $s.Text=$s.Text.slice(selLength)+$s.Text.slice(0,selLength);
      destRange=$s.GetRange(KL.Coord.serial);
      destRange.top.x-=selLength;
      if(range.direction==KL.Dir.backward){
        destRange.reverse();
      }
      $s.SetRange(destRange);
      break;
    default:
  }
}
